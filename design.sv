`include "riscv_pkg.sv"
`include "riscv_fetch.sv"
`include "riscv_decode.sv"
`include "riscv_regfile.sv"
`include "riscv_execute.sv"
`include "riscv_dmem.sv"
`include "riscv_control.sv"

module design(
    input   wire        clk,
    input   wire        reset,

    output  wire        imem_psel_o,
    output  wire        imem_penable_o,
    output  wire        imem_pwrite_o,
    output  wire[31:0]  imem_paddr_o,
    output  wire[31:0]  imem_pwdata_o,
    input   wire        imem_pready_i,
    input   wire[31:0]  imem_prdata_i,

    output  wire        dmem_psel_o,
    output  wire        dmem_penable_o,
    output  wire        dmem_pwrite_o,
    output  wire[31:0]  dmem_paddr_o,
    output  wire[31:0]  dmem_pwdata_o,
    input   wire        dmem_pready_i,
    input   wire[31:0]  dmem_prdata_i
);
    
endmodule
