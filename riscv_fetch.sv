//APB master fro the instruction fetch
module riscv_fetch(
  input		wire 		clk,
  input 	wire		reset,
  
  input 	wire 		instr_done_i,
  
  //APB interface to memory
  output	wire		psel_o,
  output	wire		penable_o,
  output	wire[31:0]	paddr_o,
  output	wire		pwrite_o,
  output	wire[31:0]	pwdata_o,
  input		wire		pready_i,
  input		wire		prdata_i 
  
  //instruction output
  output	logic		if_dec_valid_o,
  output	logic[31:0] if_dec_instr_o,
  input		wire		ex_if_pc_i
  
);
  //Enum for the APB state
  typedef enum logic[1:0] { ST_IDLE = 2'b00, ST_SETUP = 2'b01, ST_ACCESS = 2'b10 }apb_state_t;
  
  apb_state_t 	state_q;
  apb_state_t 	nxt_state;
  logic[31:0] 	if_pc_q;
  logic 		nxt_dec_valid;
  
  always_ff @(posedge clk or posedge reset)
    if(reset)
      state_q <= ST_IDLE;
  	else
      state_q <= nxt_state;
  
  always_comb begin
    nxt_state = state_q;
    case(state_q)
      //Memory responses may not be single cycle, wait for the memory request to compelte before requesting for the next instruction
      ST_IDLE 		: nxt_state = instr_done_i ? ST_SETUP : ST_IDLE;
      ST_SETUP		: nxt_state = ST_ACCESS;
      ST_ACCESS	: if(pready_i) nxt_state = ST_IDLE;       
    endcase
  end
  
  assign psel_o 	= (state_q == ST_SETUP) || (state_q == ST_ACCESS);
  assign penable_o	= (state_q == ST_ACCESS);
  assign paddr_o	= if_pc_q;//current instruction counter
  assign pwrite_o	= 1'b0;//No writes to IMEM
  assign pwdata_o	= 32'h0;
  
  //start from 0x8000_0000 on reset
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      if_pc_q <= 32'h8000_0000;
    else
      if_pc_q <= ex_if_pc_i;
  end
  
  //capture the read data as the instruction OPCODE
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      if_dec_instr_o <= 32'h0;
    else if(penable_o && pready_i)
      if_dec_instr_o <= prdata_i;
  end
  
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      if_dec_valid_o <= 32'h0;
    else
      if_dec_valid_o <= nxt_dec_valid;
  end
  
  //set valid on:
  	//valid APB txn and clear it next cycle if no valid tfer 
  	//and if the flag was set
  assign nxt_dec_valid = peanble_o && pready_i;
      
endmodule
